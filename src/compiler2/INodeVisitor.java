package compiler2;

public interface INodeVisitor
{
	public abstract void Visit(INode node);
	public abstract void Visit(ValueNode node);
	public abstract void Visit(OperatorNode operatorNode);
	public abstract void Visit(IFNode ifNode);
	public abstract void Visit(BlockNode blockNode);
	public abstract void Visit(FunctionNode funcNode);
	public abstract void Visit(VariableNode variableNode);
	public abstract void Visit(FunCallNode funCallNode);
	public abstract void Visit(AssignmentNode assignmentNode);
	public abstract void Visit(ReturnNode returnNode);
	public abstract void Visit(ArrayReadNode arrayReadNode);
	public abstract void Visit(ArrayWriteNode arrayWriteNode);
	public abstract void Visit(PokeNode pokeNode);
	public abstract void Visit(LoopNode loopNode);
}
