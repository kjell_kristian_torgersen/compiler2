package compiler2;

public final class VariableNode implements INode
{
	private final String name;
	
	public VariableNode(String name) 
	{
		this.name = name;
	} 

	public String GetName() 
	{
		return name;
	}
	
	@Override
	public INode GetLeft()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public INode GetRight()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void Accept(INodeVisitor visitor)
	{
		visitor.Visit(this);

	}

}
