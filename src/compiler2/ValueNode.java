package compiler2;

public class ValueNode implements INode
{
	private final int value;
	
	public ValueNode(int value)
	{
		this.value = value;
	}
	
	public int GetValue()
	{
		return value;
	}

	@Override
	public INode GetLeft()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public INode GetRight()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void Accept(INodeVisitor visitor)
	{
		visitor.Visit(this);
	}
}
