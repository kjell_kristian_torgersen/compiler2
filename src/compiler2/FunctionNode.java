package compiler2;

import java.util.ArrayList;
import java.util.List;

public final class FunctionNode implements INode
{
	private final String name;
	private final List<String> args;
	private final BlockNode block;

	public FunctionNode(String name,  List<String> args, BlockNode block)
	{
		this.name = name;
		this.args = args;
		this.block = block;
	}

	public String GetName() 
	{
		return name;
	}
	
	public List<String> GetLocals() 
	{
		return block.GetVariables();
	}
	
	public List<String> GetArgs()
	{
		return args;
	}

	public BlockNode GetBlock()
	{
		return block;
	}

	@Override
	public INode GetLeft()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public INode GetRight()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void Accept(INodeVisitor visitor)
	{
		visitor.Visit(this);

	}

}
