package compiler2;

import java.util.NoSuchElementException;
import java.util.Stack;

public class InOrderIterator implements NodeIterator
{
	Stack<INode> stack;

	/** Push node cur and all of its left children into stack */
	private void PushLeftChildren(INode cur)
	{
		
		while (cur != null) {
			stack.push(cur);
			cur = cur.GetLeft();
		}
	}

	/** Constructor */
	public InOrderIterator(INode root)
	{
		stack = new Stack<INode>();
		PushLeftChildren(root);
	}

	@Override
	public boolean HasNext()
	{
		return !stack.isEmpty();
	}

	@Override
	public INode Next()
	{
		if (!HasNext()) {
			throw new NoSuchElementException("All nodes have been visited!");
		}

		INode res = stack.pop();
		PushLeftChildren(res.GetRight());

		return res;
	}

}
