package compiler2;

public class Token
{
	private final Tokens token;
	private final String str;
	private final int value;

	public Token(Tokens token, String str, int value)
	{
		this.token = token;
		this.str = str;
		this.value = value;
	}

	public Tokens GetToken()
	{
		return token;
	}

	public String GetStr()
	{
		return str;
	}

	public int GetValue()
	{
		return value;
	}

	public String toString()
	{
		if ((str != null)) {
			if (token == Tokens.STRING) {
				return "\"" + str + "\"";
			} else {
				if (str.equals(";") || str.equals("{")) {
					return str;
				} else {
					return str;
				}
			}

		} else {
			return "" + value;
		}
	}
}
