package compiler2;

import java.util.NoSuchElementException;
import java.util.Stack;

public class PostsOrderIterator implements NodeIterator
{
	Stack<INode> stack;

	public PostsOrderIterator(INode root)
	{
		stack = new Stack<INode>();
		FindNextLeaf(root);
	}

	private void FindNextLeaf(INode cur)
	{
		while (cur != null) {
			stack.push(cur);
			if (cur.GetLeft() != null) {
				cur = cur.GetLeft();
			} else {
				cur = cur.GetRight();
			}
		}
	}

	@Override
	public boolean HasNext()
	{

		return !stack.isEmpty();
	}

	@Override
	public INode Next()
	{
		if (!HasNext()) {
			throw new NoSuchElementException("All nodes have been visited!");
		}

		INode res = stack.pop();
		if (!stack.isEmpty()) {
			INode top = stack.peek();
			if (res == top.GetLeft()) {
				FindNextLeaf(top.GetRight()); // find next leaf in
								// right
								// sub-tree
			}
		}

		return res;
	}

}
