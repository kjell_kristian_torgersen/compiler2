package compiler2;

public class OperatorNode implements INode
{
	private INode left;
	private final Operators operator;
	private INode right;

	public OperatorNode(Operators operator)
	{
		this.operator = operator;
	}

	public OperatorNode(Operators op, INode a, INode b)
	{
		this.operator = op;
		this.left = a;
		this.right = b;
	}

	public void SetLeft(INode node)
	{
		this.left = node;
	}

	public void SetRight(INode node)
	{
		this.right = node;
	}

	public INode GetLeft()
	{
		return left;
	}

	public INode GetRight()
	{
		return right;
	}

	public Operators GetOperator()
	{
		return operator;
	}

	@Override
	public void Accept(INodeVisitor visitor)
	{
		visitor.Visit(this);
	}
}
