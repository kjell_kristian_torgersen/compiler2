package compiler2;

import java.util.ArrayList;
import java.util.List;

public class ASTBuilder
{

	// Lexer lex;
	private List<Token> tokens;
	private int current = 0;
	private List<String> variables;
	private List<String> strings;
	private List<FunctionNode> functions;
	private String currentFunction = null;

	public List<FunctionNode> GetFunctions()
	{
		return functions;
	}

	public List<String> GetVariables()
	{
		return variables;
	}

	private boolean HasNext()
	{
		return current < tokens.size();
	}

	private Token Look()
	{
		if (current < tokens.size())
			return tokens.get(current);
		else
			return new Token(Tokens.NULL, "", 0);
	}

	private Token Look(int rel)
	{
		return tokens.get(current + rel);
	}

	private void Next()
	{
		current++;
	}

	public OperatorNode OperatorNodeFactory(char operator)
	{
		switch (operator) {
		case '+':
			return new OperatorNode(Operators.ADD);
		case '-':
			return new OperatorNode(Operators.SUB);
		case '*':
			return new OperatorNode(Operators.MUL);
		case '/':
			return new OperatorNode(Operators.DIV);
		case '%':
			return new OperatorNode(Operators.MOD);
		case '&':
			return new OperatorNode(Operators.AND);
		case '|':
			return new OperatorNode(Operators.OR);
		case '^':
			return new OperatorNode(Operators.XOR);
		default:
			return null;
		}
	}

	private void Match(Tokens x)
	{
		if (Look().GetToken() != x) {
			System.out.println("Expected: " + x + " but got " + Look().GetToken());
		} else {
			Next();
		}
	}

	private INode Ident()
	{
		String name = Look().GetStr();
		Next();
		if (Look().GetToken() == Tokens.LPAREN) {
			List<INode> arguments = new ArrayList<INode>();
			Match(Tokens.LPAREN);
			if (Look().GetToken() != Tokens.RPAREN) {
				while (Look().GetToken() != Tokens.RPAREN) {
					arguments.add(Expression());
					if (Look().GetToken() == Tokens.COMMA)
						Match(Tokens.COMMA);
				}
			}
			Match(Tokens.RPAREN);
			return new FunCallNode(name, arguments);
			// EmitLn("call " + Name);
		} else if (Look().GetToken() == Tokens.LBRACK) { // array
			Match(Tokens.LBRACK);
			INode index = Expression();
			Match(Tokens.RBRACK);
			return new ArrayReadNode(name, index);
		} else {
			return new VariableNode(name);
		}
	}

	private INode Factor()
	{
		INode A = null;

		if (Look().GetToken() == Tokens.LPAREN) {
			Match(Tokens.LPAREN);
			A = Expression();
			Match(Tokens.RPAREN);
		} else if (Look().GetToken() == Tokens.IDENT) {
			A = Ident();
		} else if (Look().GetToken() == Tokens.CONSTANT) {
			A = new ValueNode(Look().GetValue());
			Next();
		}
		return A;
	}

	/*
	 * private INode Multiply(INode factor1) { Match('*'); return new
	 * OperatorNode(Operators.MUL); //EmitLn("pop bx");
	 * //EmitLn("muls ax,bx"); }
	 * 
	 * private INode Divide(INode factor1) { Match('/'); return Factor();
	 * //EmitLn("pop bx"); //EmitLn("divs ax,bx"); }
	 */

	private INode Term()
	{
		INode root = Factor();
		while ((Look().GetToken() == Tokens.MUL) || ((Look().GetToken() == Tokens.DIV))) {
			// EmitLn("push ax");
			switch (Look().GetToken()) {
			case MUL:
				Match(Tokens.MUL);
				root = new OperatorNode(Operators.MUL, root, Factor());
				break;
			// Multiply();

			case DIV:
				Match(Tokens.DIV);
				root = new OperatorNode(Operators.DIV, root, Factor());
				break;

			default:
				// Expected("Mulop");
			}
		}
		return root;
	}

	/*
	 * private INode Add() { Match('+'); return Term(); //EmitLn("pop bx");
	 * //EmitLn("add ax,bx"); }
	 * 
	 * private INode Subtract() { Match('-'); return Term(); //
	 * EmitLn("pop bx"); // EmitLn("sub ax,bx"); // EmitLn("neg ax"); }
	 */

	public boolean IsExpressionOperator(Tokens token)
	{
		boolean result = false;
		if (token == Tokens.PLUS)
			result = true;
		else if (token == Tokens.MINUS)
			result = true;
		else if (token == Tokens.AND)
			result = true;
		else if (token == Tokens.OR)
			result = true;
		else if (token == Tokens.XOR)
			result = true;
		else if (token == Tokens.EQUAL)
			result = true;
		else if (token == Tokens.LESS)
			result = true;
		else if (token == Tokens.GREATER)
			result = true;
		else if (token == Tokens.LEQ)
			result = true;
		else if (token == Tokens.GEQ)
			result = true;
		else if (token == Tokens.NEQ)
			result = true;
		return result;
	}

	private INode Expression()
	{
		INode root = null;
		// if (IsAddop(lex.Look())) {
		// EmitLn("mov ax,0");
		// } else {
		root = Term();
		// }

		if (Look().GetToken() == Tokens.STRING) {
			strings.add(Look().GetStr());
			root = new VariableNode("_str" + (strings.size() - 1));
			Match(Tokens.STRING);
		} else {

			while (IsExpressionOperator(Look().GetToken())) {
				// EmitLn("push ax");
				switch (Look().GetToken()) {
				case PLUS:
					Match(Tokens.PLUS);
					root = new OperatorNode(Operators.ADD, root, Term());
					break;
				// INode op = Add();

				case MINUS:
					Match(Tokens.MINUS);
					root = new OperatorNode(Operators.SUB, root, Term());
					break;
				case AND:
					Match(Tokens.AND);
					root = new OperatorNode(Operators.AND, root, Term());
					break;
				case OR:
					Match(Tokens.OR);
					root = new OperatorNode(Operators.OR, root, Term());
					break;
				case XOR:
					Match(Tokens.XOR);
					root = new OperatorNode(Operators.XOR, root, Term());
					break;
				case GREATER:
					Match(Tokens.GREATER);
					root = new OperatorNode(Operators.GREATER, root, Term());
					break;
				case LESS:
					Match(Tokens.LESS);
					root = new OperatorNode(Operators.LESS, root, Term());
					break;
				case GEQ:
					Match(Tokens.GEQ);
					root = new OperatorNode(Operators.GEQ, root, Term());
					break;
				case LEQ:
					Match(Tokens.LEQ);
					root = new OperatorNode(Operators.LEQ, root, Term());
					break;
				case EQUAL:
					Match(Tokens.EQUAL);
					root = new OperatorNode(Operators.EQUAL, root, Term());
					break;
				case NEQ:
					Match(Tokens.NEQ);
					root = new OperatorNode(Operators.NEQ, root, Term());
					break;
				default:
					// Expected("Addop");
					break;
				}
				// Next();
			}
		}
		return root;

	}

	private BlockNode Block()
	{
		BlockNode root = new BlockNode();
		Match(Tokens.LBRACE);
		while (Look().GetToken() != Tokens.RBRACE) {
			if (Look().GetToken() == Tokens.VAR) {
				Match(Tokens.VAR);
				while (Look().GetToken() != Tokens.SEMICOLON) {
					root.GetVariables().add(Look().GetStr());
					Next();
				}
				Match(Tokens.SEMICOLON);
			} else {
				root.AddStatement(Statement());
			}
		}
		Match(Tokens.RBRACE);
		return root;
	}

	public void BuildTree(String input)
	{
		variables = new ArrayList<String>();
		functions = new ArrayList<FunctionNode>();
		strings = new ArrayList<String>();

		tokens = new ArrayList<Token>();
		LangTokenizer lt = new LangTokenizer(input);
		while (lt.HasNext()) {
			tokens.add(lt.GetToken());
		}
		Program(); // Expression();
		return;
	}

	private INode Statement()
	{
		INode statement = null;
		if (Look().GetToken() == Tokens.IF) {
			Match(Tokens.IF);
			Match(Tokens.LPAREN);
			INode condition = Expression();
			Match(Tokens.RPAREN);
			BlockNode block = Block();
			statement = new IFNode(condition, block);
		} else if (Look().GetToken() == Tokens.RETURN) {
			Match(Tokens.RETURN);
			statement = new ReturnNode(currentFunction, Expression());
			Match(Tokens.SEMICOLON);
		} else if ((Look().GetToken() == Tokens.IDENT) && (Look(1).GetToken() == Tokens.ASSIGNMENT)) {
			String name = Look().GetStr();
			Match(Tokens.IDENT);
			if (Look().GetToken() == Tokens.LBRACK) {
				Match(Tokens.LBRACK);
				INode index = Expression();
				Match(Tokens.RBRACK);
				Match(Tokens.ASSIGNMENT);
				INode expression = Expression();
				statement = new ArrayWriteNode(name, index, expression);
			} else {
				Match(Tokens.ASSIGNMENT);
				INode expression = Expression();
				statement = new AssignmentNode(name, expression);
			}
			Match(Tokens.SEMICOLON);
		} else if (Look().GetToken() == Tokens.POKE) {
			Match(Tokens.POKE);
			Match(Tokens.LPAREN);
			INode segment = Expression();
			Match(Tokens.COMMA);
			INode address = Expression();
			Match(Tokens.COMMA);
			INode value = Expression();
			Match(Tokens.RPAREN);
			Match(Tokens.SEMICOLON);
			statement = new PokeNode(segment, address, value);
		} else if (Look().GetToken() == Tokens.WHILE) {
			Match(Tokens.WHILE);
			Match(Tokens.LPAREN);
			INode condition = Expression();
			Match(Tokens.RPAREN);
			BlockNode block = Block();
			statement = new LoopNode(null, condition, null, block);
		} else if (Look().GetToken() == Tokens.FOR) {
			INode init = null;
			INode condition = null;
			INode increment = null;
			Match(Tokens.FOR);
			Match(Tokens.LPAREN);

			if (Look().GetToken() != Tokens.SEMICOLON) {
				init = Statement();
			} else {
				Match(Tokens.SEMICOLON);
			}
			if (Look().GetToken() != Tokens.SEMICOLON) {
				condition = Expression();
			}
			Match(Tokens.SEMICOLON);

			if (Look().GetToken() != Tokens.RPAREN) {
				increment = Statement();
			}
			Match(Tokens.RPAREN);
			BlockNode block = Block();
			statement = new LoopNode(init, condition, increment, block);
		} else {

			statement = Expression();
			Match(Tokens.SEMICOLON);
		}
		return statement;
	}

	private void Program()
	{

		while (HasNext()) {
			if (Look().GetToken() == Tokens.VAR) {
				Match(Tokens.VAR);
				while (Look().GetToken() != Tokens.SEMICOLON) {
					variables.add(Look().GetStr());
					Next();
				}
				Match(Tokens.SEMICOLON);
			} else if (Look().GetToken() == Tokens.FUNC) {
				Match(Tokens.FUNC);
				String name = Look().GetStr();
				Next();
				List<String> args = new ArrayList<String>();
				Match(Tokens.LPAREN);
				while (Look().GetToken() != Tokens.RPAREN) {
					args.add(Look().GetStr());
					Next();
					if (Look().GetToken() == Tokens.COMMA)
						Match(Tokens.COMMA);
				}
				Match(Tokens.RPAREN);
				currentFunction = name;
				functions.add(new FunctionNode(name, args, Block()));
			}
		}
	}

	public List<String> GetStrings()
	{
		return strings;
	}
}
