package compiler2;

import java.util.List;

public class PrintVisitor implements INodeVisitor
{
	int level = 0;

	@Override
	public void Visit(ValueNode node)
	{
		System.out.print(node.GetValue() + " ");
	}

	@Override
	public void Visit(OperatorNode operatorNode)
	{
		switch (operatorNode.GetOperator()) {
		case ADD:
			System.out.print("+");
			break;
		case SUB:
			System.out.print("-");
			break;
		case MUL:
			System.out.print("*");
			break;
		case DIV:
			System.out.print("/");
			break;
		case MOD:
			System.out.print("%");
			break;
		case AND:
			System.out.print("&");
			break;
		case OR:
			System.out.print("|");
			break;
		case XOR:
			System.out.print("^");
			break;
		case EQUAL:
			System.out.print("==");
			break;
		case GREATER:
			System.out.print(">");
			break;
		case LESS:
			System.out.print("<");
			break;
		case GEQ:
			System.out.print(">=");
			break;
		case LEQ:
			System.out.print("<=");
			break;
		default:
			break;
		}
		System.out.print(" ");

	}

	@Override
	public void Visit(INode node)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void Visit(IFNode ifNode)
	{
		PostsOrderIterator it = new PostsOrderIterator(ifNode.GetCondition());
		PrintVisitor pv = new PrintVisitor();
		System.out.print("if (");
		while (it.HasNext()) {
			it.Next().Accept(pv);
		}
		System.out.print(") ");
		Visit(ifNode.GetBlock());
	}

	@Override
	public void Visit(BlockNode blockNode)
	{
		System.out.print("{");
		level++;
		System.out.println();
		List<INode> statements = blockNode.GetStatements();
		for (INode stmt : statements) {
			PostsOrderIterator it = new PostsOrderIterator(stmt);
			//stmt.Accept(this);
			//PrintVisitor pv = new PrintVisitor();
			while (it.HasNext()) {
				it.Next().Accept(this);
			}
			System.out.print(";");
		}
		level--;
		System.out.print("}");
	}

	@Override
	public void Visit(FunctionNode funcNode)
	{
		System.out.print("func " + funcNode.GetName() + "(");
		for (String arg : funcNode.GetArgs()) {
			System.out.print(arg + " ");

		}
		System.out.println(")");
		Visit(funcNode.GetBlock());

	}

	@Override
	public void Visit(VariableNode variableNode)
	{
		// TODO Auto-generated method stub
		System.out.print(variableNode.GetName() + " ");
	}

	@Override
	public void Visit(FunCallNode funCallNode)
	{
		System.out.print(funCallNode.GetName() + "() ");

	}

	@Override
	public void Visit(AssignmentNode assignmentNode)
	{
		PostsOrderIterator it = new PostsOrderIterator(assignmentNode.GetExpression());
		while(it.HasNext()) it.Next().Accept(this);
		System.out.print(assignmentNode.GetName() + "= ");
		
	}

	@Override
	public void Visit(ReturnNode returnNode)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void Visit(ArrayReadNode arrayReadNode)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void Visit(ArrayWriteNode arrayWriteNode)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void Visit(PokeNode pokeNode)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void Visit(LoopNode loopNode)
	{
		// TODO Auto-generated method stub
		
	}

	/*
	 * public void Emit(String s) { for (int i = 0; i < level; i++) {
	 * System.out.print("\t"); } System.out.print(s); }
	 */

}
