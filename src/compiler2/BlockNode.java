package compiler2;

import java.util.ArrayList;
import java.util.List;

public class BlockNode implements INode
{
	private List<INode> statements;
	private List<String> variables;

	public BlockNode()
	{
		statements = new ArrayList<INode>();
		variables = new ArrayList<String>();
	}

	public List<String> GetVariables()
	{
		return variables;
	}

	public List<INode> GetStatements()
	{
		return statements;
	}

	public void AddStatement(INode root)
	{
		if (root != null) {
			statements.add(root);
		}
	}

	@Override
	public INode GetLeft()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public INode GetRight()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void Accept(INodeVisitor visitor)
	{
		visitor.Visit(this);

	}

}
