package compiler2;

public final class ArrayReadNode implements INode
{
	private final String variable;
	private final INode index;

	public ArrayReadNode(String variable, INode index)
	{
		this.variable = variable;
		this.index = index;
	}

	public String GetVariable()
	{
		return variable;
	}

	public INode GetIndex()
	{
		return index;
	}

	@Override
	public INode GetLeft()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public INode GetRight()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void Accept(INodeVisitor visitor)
	{
		visitor.Visit(this);
	}

}
