package compiler2;

public final class AssignmentNode implements INode
{
	private final String name;
	private final INode expression;

	public AssignmentNode(String name, INode expression)
	{
		this.name = name;
		this.expression = expression;
	}

	public String GetName()
	{
		return name;
	}

	public INode GetExpression()
	{
		return expression;
	}

	@Override
	public INode GetLeft()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public INode GetRight()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void Accept(INodeVisitor visitor)
	{
		visitor.Visit(this);
	}

}
