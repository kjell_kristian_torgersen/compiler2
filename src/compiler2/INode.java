package compiler2;

public interface INode
{
	public abstract INode GetLeft();
	public abstract INode GetRight();
	public abstract void Accept(INodeVisitor visitor);
}
