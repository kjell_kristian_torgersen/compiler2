package compiler2;

import java.util.NoSuchElementException;
import java.util.Stack;

public class PreOrderIterator implements NodeIterator
{
	private Stack<INode> stack;

	public PreOrderIterator(INode root)
	{
		stack = new Stack<INode>();
		if (root != null) {
			stack.push(root); // add to end of queue
		}
	}

	@Override
	public boolean HasNext()
	{
		return !stack.isEmpty();
	}

	@Override
	public INode Next()
	{
		if (!HasNext()) {
			throw new NoSuchElementException("All nodes have been visited!");
		}

		INode res = stack.pop(); // retrieve and remove the head of
						// queue
		if (res.GetRight() != null)
			stack.push(res.GetRight());
		if (res.GetLeft() != null)
			stack.push(res.GetLeft());

		return res;

	}

}
