package compiler2;

import java.util.ArrayList;
import java.util.List;

public class ASMVisitor implements INodeVisitor
{
	int labelnr = 0;

	List<FunctionNode> functions;
	List<String> globalVariables;
	List<String> argumentVariables;
	List<String> localVariables;
	List<String> strings;

	StringBuilder program;

	public int GetLabelNr()
	{
		return labelnr++;
	}

	public String GetProgram()
	{
		return program.toString();
	}

	private void Write(String string)
	{
		program.append(string);
		// System.out.print(string);
	}

	private void EmitTab(String s)
	{
		Emit("\t" + s);
	}

	private void Emit(String s)
	{
		Write(s + "\n");
	}

	public ASMVisitor(List<FunctionNode> functions, List<String> variables, List<String> strings)
	{
		this.functions = functions;
		this.globalVariables = variables;
		this.program = new StringBuilder();
		this.strings = strings;

		Emit("org 100h");
		Emit("section .text");
		EmitTab("; jump to program start");
		EmitTab("call start");
		EmitTab("; return to dos");
		EmitTab("mov ax,4c00h");
		EmitTab("int 21h");

		Finish();
	}

	public void Finish()
	{
		int i = 0;
		for (FunctionNode fn : functions) {
			PrintFunction(fn);
		}

		Emit("section .data");
		for (String string : strings) {
			Emit("_str" + i + "\t db \"" + string + "\", 0");
			i++;
		}
		Emit("section .bss");
		EmitTab("; global variables");
		for (String variable : globalVariables) {
			Emit(variable + "\tresw 1");
		}
	}

	@Override
	public void Visit(INode node)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void Visit(ValueNode node)
	{
		EmitTab("push " + node.GetValue());
		// EmitTab("push ax");
	}

	@Override
	public void Visit(OperatorNode operatorNode)
	{
		switch (operatorNode.GetOperator()) {
		case ADD:
			EmitTab("; Perform the addition");
			EmitTab("pop bx");
			EmitTab("pop ax");
			EmitTab("add ax,bx");
			EmitTab("push ax");
			break;
		case SUB:
			EmitTab("; Perform the subtraction");
			EmitTab("pop bx");
			EmitTab("pop ax");
			EmitTab("sub ax,bx");
			EmitTab("push ax");
			break;
		case MUL:
			EmitTab("; Perform the multiplication");
			EmitTab("pop bx");
			EmitTab("pop ax");
			EmitTab("mul bx");
			EmitTab("push ax");
			break;
		case DIV:
			EmitTab("; Perform the divisionn");
			EmitTab("pop bx");
			EmitTab("pop ax");
			EmitTab("div bx");
			EmitTab("push ax");
			break;
		case MOD:
			EmitTab("; Perform the modulus");
			EmitTab("pop bx");
			EmitTab("pop ax");
			EmitTab("mod ax,bx");
			EmitTab("push ax");
			break;
		case AND:
			EmitTab("; Perform the bitwise logical and");
			EmitTab("pop bx");
			EmitTab("pop ax");
			EmitTab("and ax,bx");
			EmitTab("push ax");
			break;
		case OR:
			EmitTab("; Perform the bitwise logical or");
			EmitTab("pop bx");
			EmitTab("pop ax");
			EmitTab("or ax,bx");
			EmitTab("push ax");
			break;
		case XOR:
			EmitTab("; Perform the bitwise logical exclusive or");
			EmitTab("pop bx");
			EmitTab("pop ax");
			EmitTab("xor ax,bx");
			EmitTab("push ax");
			break;
		case EQUAL:
			Comparison("je", "; Perform equal comparison. AX = 1 if equal, 0 else");
			break;
		case NEQ:
			Comparison("jne", "; Perform NOT equal comparison. AX = 1 if not equal, 0 else");
			break;
		case GREATER:
			Comparison("jg", "; Perform greater comparison. AX = 1 if greater, 0 else");
			break;
		case LESS:
			Comparison("jl", "; Perform lesser comparison. AX = 1 if lesser, 0 else");
			break;
		case GEQ:
			Comparison("jge", "; Perform greater or equal comparison. AX = 1 if greater or equal, 0 else");
			break;
		case LEQ:
			Comparison("jle", "; Perform lesser or equal comparison. AX = 1 if lesser or equal, 0 else");
			break;
		default:
			Error(operatorNode.GetOperator() + " handler not implemented.");
			break;
		}

	}

	private void Error(String string)
	{
		System.err.println("Error: " + string);
	}

	private void Comparison(String instruction, String comment)
	{
		String skiplabel = "skip" + GetLabelNr();
		EmitTab(comment);
		EmitTab("mov ax,1");
		EmitTab("pop bx");
		EmitTab("pop cx");
		EmitTab("cmp cx,bx");
		EmitTab(instruction + " " + skiplabel);
		EmitTab("mov ax,0");
		Emit(skiplabel + ":");
		EmitTab("push ax");
	}

	@Override
	public void Visit(IFNode ifNode)
	{
		String skiplabel = "if_skip" + GetLabelNr();
		EmitTab("; Executing condition in if-statement");
		ExecuteExpression(ifNode.GetCondition());
		EmitTab("; Deciding whether to execute if-block");
		EmitTab("add sp,2");
		EmitTab("test ax,ax");
		EmitTab("jz " + skiplabel);
		EmitTab("; Start of code block belonging to if");
		Visit(ifNode.GetBlock());
		EmitTab("; End of code block belonging to if");
		Emit(skiplabel + ":");
		// Emit("pop bx");
		// Emit(s)
	}

	@Override
	public void Visit(BlockNode blockNode)
	{
		PostsOrderIterator it;
		ASMVisitor av;
		for (INode root : blockNode.GetStatements()) {
			ExecuteExpression(root);
		}
	}

	void PrintFunction(FunctionNode funcNode)
	{
		localVariables = funcNode.GetLocals();
		argumentVariables = funcNode.GetArgs();

		Write("; function " + funcNode.GetName() + "(");
		for (String args : funcNode.GetArgs()) {
			Write(args + " ");
		}
		Emit(")");
		Emit(funcNode.GetName() + ":");
		EmitTab("; Set up stack frame");
		EmitTab("push bp");
		EmitTab("mov bp,sp");
		if (funcNode.GetLocals().size() != 0) {
			Write("\t; Make room on the stack for local variables");
			for (String var : funcNode.GetLocals()) {
				Write(" " + var);
			}
			Emit("");
			EmitTab("sub sp," + 2 * funcNode.GetLocals().size());
		}
		Visit(funcNode.GetBlock());
		Emit(funcNode.GetName() + "_end:");
		EmitTab("; Destroy stack frame");
		EmitTab("mov sp,bp");
		EmitTab("pop bp");
		EmitTab("; return from " + funcNode.GetName());
		EmitTab("ret");

	}

	@Override
	public void Visit(FunctionNode funcNode)
	{
		// functions.add(funcNode);
	}

	public void ReadVariable(String varname)
	{
		if (localVariables.contains(varname)) {
			int idx = localVariables.indexOf(varname);
			// get a local variable from stack
			EmitTab("; Read from local variable " + varname);
			// EmitTab("push ax");
			EmitTab("mov ax,[bp-" + 2 * (idx + 1) + "]");
			EmitTab("push ax");
		} else if (argumentVariables.contains(varname)) {
			int idx = argumentVariables.indexOf(varname);
			// get a argument variable from stack
			EmitTab("; Read from argument variable " + varname);
			EmitTab("mov ax,[bp+" + 2 * ((argumentVariables.size() - 1) - idx + 2) + "]");
			EmitTab("push ax");
		} else if (globalVariables.contains(varname)) {
			EmitTab("; Read from global variable " + varname);
			EmitTab("mov ax, [" + varname + "]\t");
			EmitTab("push ax");
		} else {
			int idx = Integer.parseInt(varname.substring(4));
			EmitTab("push _str" + idx);
		}
	}

	@Override
	public void Visit(VariableNode variableNode)
	{
		String varname = variableNode.GetName();
		ReadVariable(varname);
	}

	@Override
	public void Visit(FunCallNode funCallNode)
	{
		int arguments = 0;
		EmitTab("; prepare for calling function " + funCallNode.GetName());
		EmitTab("; execute arguments and push them to stack");
		for (INode argument : funCallNode.GetArguments()) {
			EmitTab("; preparing argument " + (arguments + 1));
			ExecuteExpression(argument);
			arguments++;
		}
		// Emit("; Save old ax value");
		// EmitTab("push ax");
		EmitTab("; calling function " + funCallNode.GetName());
		EmitTab("call " + funCallNode.GetName());
		EmitTab("; remove arguments from stack");
		EmitTab("add sp," + 2 * arguments);
		// Emit("; Restore old ax value");
		// EmitTab("pop ax");

	}

	private void ExecuteExpression(INode expression)
	{
		PostsOrderIterator it = new PostsOrderIterator(expression);
		while (it.HasNext()) {
			it.Next().Accept(this);
		}
	}

	@Override
	public void Visit(AssignmentNode assignmentNode)
	{
		String varname = assignmentNode.GetName();
		ExecuteExpression(assignmentNode.GetExpression());

		if (localVariables.contains(varname)) {
			int idx = localVariables.indexOf(varname);
			// get a local variable from stack
			EmitTab("; Write to local variable " + varname);
			EmitTab("pop ax");
			EmitTab("mov [bp-" + 2 * (idx + 1) + "], ax");
		} else if (argumentVariables.contains(varname)) {
			int idx = argumentVariables.indexOf(varname);
			// get a argument variable from stack
			EmitTab("; Write to argument variable " + varname);
			EmitTab("pop ax");
			EmitTab("mov [bp+" + 2 * ((argumentVariables.size() - 1) + idx + 2) + "], ax");

		} else if (globalVariables.contains(varname)) {
			// get a global variable

			EmitTab("; Write to global variable " + varname);
			EmitTab("pop ax");
			EmitTab("mov [" + varname + "],ax");
		}
	}

	@Override
	public void Visit(ReturnNode returnNode)
	{
		EmitTab("; Calculating return value for function");
		ExecuteExpression(returnNode.GetExpression());
		EmitTab("pop ax");
		EmitTab("; Returning from this function");
		EmitTab("jmp " + returnNode.GetName() + "_end");
	}

	@Override
	public void Visit(ArrayReadNode arrayReadNode)
	{
		ReadVariable(arrayReadNode.GetVariable());
		ExecuteExpression(arrayReadNode.GetIndex());
		EmitTab("pop bx");
		EmitTab("pop ax");
		EmitTab("add bx,ax");
		EmitTab("mov ax,0");
		EmitTab("mov al,[bx]");
		EmitTab("push ax");
	}

	@Override
	public void Visit(ArrayWriteNode arrayWriteNode)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void Visit(PokeNode pokeNode)
	{
		ExecuteExpression(pokeNode.GetSegment());
		ExecuteExpression(pokeNode.GetAddress());
		ExecuteExpression(pokeNode.GetValue());
		EmitTab("pop ax"); // value
		EmitTab("pop bx"); // address
		EmitTab("pop es"); // segment
		EmitTab("mov [es:bx],al");
	}

	@Override
	public void Visit(LoopNode loopNode)
	{
		int labelnr = GetLabelNr();
		EmitTab("; Beginning of a loop");

		if (loopNode.GetInit() != null) {
			EmitTab("; Evaluate initialization");
			ExecuteExpression(loopNode.GetInit());
		}
		Emit("loop" + labelnr + ":");
		if (loopNode.GetInit() != null) {
			EmitTab("; Evaluate condition");
			ExecuteExpression(loopNode.GetCondition());
			EmitTab("; Check if done with loop");
			EmitTab("pop ax");
			EmitTab("test ax,ax");
			// EmitTab("pop ax");
			EmitTab("; Yes, jump to end");
			EmitTab("jz endloop" + labelnr);
			EmitTab("; Else, evaluate block");
		}
		
		Visit(loopNode.GetBlock());
		if (loopNode.GetIncrement() != null) {
			EmitTab("; Perform iteration/increment");
			ExecuteExpression(loopNode.GetIncrement());
		}
		EmitTab("; Go back to start");
		EmitTab("jmp loop" + labelnr);
		Emit("endloop" + labelnr + ":");

	}
}
