package compiler2;

public enum Operators 
{
	ADD, SUB, MUL, DIV, MOD, AND, OR, XOR, GEQ, LEQ, GREATER, LESS, EQUAL, NEQ
}
