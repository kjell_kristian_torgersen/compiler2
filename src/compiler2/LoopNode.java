package compiler2;

public final class LoopNode implements INode
{
	private final INode init;
	private final INode condition;
	private final INode increment;
	private final BlockNode block;

	public LoopNode(INode init, INode condition, INode increment, BlockNode block)
	{
		this.init = init;
		this.condition = condition;
		this.increment = increment;
		this.block = block;
	}

	public INode GetInit()
	{
		return init;
	}

	public INode GetCondition()
	{
		return condition;
	}

	public INode GetIncrement()
	{
		return increment;
	}

	public BlockNode GetBlock()
	{
		return block;
	}

	@Override
	public INode GetLeft()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public INode GetRight()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void Accept(INodeVisitor visitor)
	{
		visitor.Visit(this);
	}

}
