package compiler2;

public interface NodeIterator
{
	public boolean HasNext();
	public INode Next();

}
