package compiler2;

import java.util.List;

public final class FunCallNode implements INode
{
	private final String name;
	private final List<INode> arguments;

	public FunCallNode(String name, List<INode> arguments)
	{
		this.name = name;
		this.arguments = arguments;
	}

	public List<INode> GetArguments()
	{
		return arguments;
	}

	public String GetName()
	{
		return name;
	}

	@Override
	public INode GetLeft()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public INode GetRight()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void Accept(INodeVisitor visitor)
	{
		visitor.Visit(this);

	}

}
