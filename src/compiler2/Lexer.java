package compiler2;

public class Lexer
{
	private String Input = null;
	private int idx = 0;

	private boolean IsAlpha(char c)
	{
		return Character.isAlphabetic(c);
	}
	
	private boolean IsDigit(char c)
	{
		return Character.isDigit(c);
	}
	
	private boolean IsAlNum(char c)
	{
		return (IsAlpha(c) || IsDigit(c));
	}
	
	public String GetName()
	{
		String Token;
		Token = "";
		if (!IsAlpha(Look()))
			Expected("Name");
		while (IsAlNum(Look())) {
			Token += Character.toUpperCase(Look());
			GetChar();
		}
		SkipWhite();
		return Token;
	}
	
	public void SkipWhite()
	{
		while(Look() == ' ') GetChar();
	}
	
	public Lexer(String input)
	{
		this.Input = input;
	}

	public char Look()
	{
		if(idx >= Input.length()) return 0;
		return Input.charAt(idx);
	}

	public void Next()
	{
		idx++;
	}

	public char GetChar()
	{
		char c = Look();
		Next();
		return c;
	}
	
	public int GetNum()
	{
		int value = 0;
		if (Character.isDigit(Look())) {
			while (Character.isDigit(Look())) {
				value *= 10;
				value += Look()-'0';
				Next();
			}
			SkipWhite();
		} else {
			Expected("Integer");
		}
		return value;
	}

	private void Expected(String string) 
	{
		System.out.println("Expected " + string);

	}

}
