package compiler2;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.ArrayList;

public class Main
{
	public static void WriteStringToFile(String filename, String string)
	{
		try (PrintWriter out = new PrintWriter(filename)) {
			out.println(string);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static String GetAllLines(String filename)
	{
		try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
				line = br.readLine();
			}
			String everything = sb.toString();
			return everything;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		String outfile = "/home/kjell/workspace-java/Compiler2/program.asm";
		String comfile = "/home/kjell/dos/program.com";
		String program = GetAllLines("/home/kjell/workspace-java/Compiler2/program.k");
		ASTBuilder ast = new ASTBuilder();
		// PrintVisitor pv = new PrintVisitor();
		// "var v1 v2 v3;func other(arg1,arg2) {v1=arg1+arg2;return v1;} func start(arg) { if(2>1) {other(1,2);}}"
		ast.BuildTree(program);
		ASMVisitor av = new ASMVisitor(ast.GetFunctions(), ast.GetVariables(), ast.GetStrings());
		WriteStringToFile("/home/kjell/workspace-java/Compiler2/program.asm", av.GetProgram());
		try {
			Process p = Runtime.getRuntime().exec("nasm " + outfile + " -o " + comfile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.print(av.GetProgram());
		/*
		 * NodeIterator ni = new PostsOrderIterator(root); while
		 * (ni.HasNext()) { ni.Next().Accept(av); }
		 */

		// LangTokenizer lt = new
		// LangTokenizer("var a,b=0,c;\nfunc hello(arg1,arg2,arg3) { var loc1,loc2,loc3; print(\"Hello World\",123);if(a>=b){b=c;}}");
		// Token tok;
		// int level = 0;
		// String s;
		/*
		 * while (lt.HasNext()) { tok = lt.GetToken(); s = tok.GetStr();
		 * if (s.equals("}")) { level--; }
		 * 
		 * System.out.print(tok.toString() + " "); if (s.equals("{"))
		 * level++;
		 * 
		 * if (s.equals(";") || s.equals("{") || s.equals("}")) {
		 * System.out.println(); for (int i = 0; i < level; i++)
		 * System.out.print("\t"); }
		 * 
		 * }
		 */
	}

}
