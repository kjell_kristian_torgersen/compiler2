package compiler2;

public class IFNode implements INode
{
	public final INode condition;
	public final BlockNode block;
	
	public IFNode(INode condition, BlockNode block) 
	{
		this.condition = condition;
		this.block = block;
	}
	public INode GetCondition() 
	{
		return condition;
	}
	
	public BlockNode GetBlock() 
	{
		return block;
	}
	
	@Override
	public INode GetLeft()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public INode GetRight()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void Accept(INodeVisitor visitor)
	{ 
		visitor.Visit(this);
	}

}
