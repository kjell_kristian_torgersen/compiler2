package compiler2;

public final class ArrayWriteNode implements INode
{
	private final String name;
	private final INode index;
	private final INode expression;

	public ArrayWriteNode(String name, INode index, INode expression)
	{
		this.name = name;
		this.index = index;
		this.expression = expression;
	}

	public String GetName()
	{
		return name;
	}

	public INode GetIndex()
	{
		return index;
	}
	
	public INode GetExpression()
	{
		return expression;
	}

	@Override
	public INode GetLeft()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public INode GetRight()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void Accept(INodeVisitor visitor)
	{
		visitor.Visit(this);
	}

}
