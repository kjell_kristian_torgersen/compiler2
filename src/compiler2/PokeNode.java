package compiler2;

public final class PokeNode implements INode
{
	private final INode segment;
	private final INode address;
	private final INode value;

	public PokeNode(INode segment, INode address, INode value)
	{
		this.segment = segment;
		this.address = address;
		this.value = value;
	}

	public INode GetSegment()
	{
		return segment;
	}

	public INode GetAddress()
	{
		return address;
	}

	public INode GetValue()
	{
		return value;
	}

	@Override
	public INode GetLeft()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public INode GetRight()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void Accept(INodeVisitor visitor)
	{
		visitor.Visit(this);
	}

}
