package compiler2;

enum Tokens {
	NULL, CONSTANT, IDENT, STRING, LPAREN, RPAREN, COMMA, PLUS, MINUS, MUL, DIV, MOD, AND, OR, XOR, NOT, IF, ELSE, FUNC, LBRACE, RBRACE, SEMICOLON, VAR, EQUAL, GREATER, LESS, GEQ, LEQ, NEQ, RETURN, ASSIGNMENT, EXCLAMATION, LBRACK, RBRACK, PEEK, POKE, WHILE, FOR;
}

public class LangTokenizer
{
	private final String input;
	private int idx = 0;

	private char Look(int n)
	{
		return input.charAt(idx + n);
	}

	private char Look()
	{
		if (idx >= input.length())
			return '\0';
		else
			return input.charAt(idx);
	}

	private void SkipWS()
	{
		while (Character.isWhitespace(Look())) {
			Next();
		}
	}

	private void NextWS()
	{
		do {
			idx++;
		} while (Character.isWhitespace(Look()));

	}

	private void Next()
	{
		idx++;
	}

	private String GetName()
	{
		String ret = "";
		while (Character.isAlphabetic(Look()) || Character.isDigit(Look())) {
			ret += Look();
			Next();
		}
		SkipWS();
		return ret;
	}

	private String GetNum()
	{
		String ret = "";
		while (Character.isDigit(Look())) {
			ret += Look();
			Next();
		}
		SkipWS();
		return ret;
	}

	public LangTokenizer(String input)
	{
		this.input = input;
	}

	public boolean HasNext()
	{
		if (idx < input.length())
			return true;
		else
			return false;
	}

	public Token GetToken()
	{
		Token tok = null;
		if (Look() == '\'' && Look(2) == '\'') {
			Next();
			tok = new Token(Tokens.CONSTANT, "" + Look(), (int) Look());
			Next();
			Next();
		} else if (Character.isDigit(Look())) {
			if ((Look() == '0') && (Look(1) == 'x')) {
				Next();
				Next();
				String val = GetHex();
				tok = new Token(Tokens.CONSTANT, val, Integer.parseInt(val, 16));
			} else {
				String val = GetNum();
				tok = new Token(Tokens.CONSTANT, val, Integer.parseInt(val));
			}

		} else if (Character.isAlphabetic(Look())) {
			tok = Ident();
		} else if (Look() == '"') {
			return GetString();
		} else {
			switch (Look()) {
			case '+':
				tok = new Token(Tokens.PLUS, "+", 0);
				NextWS();
				break;
			case '-':
				tok = new Token(Tokens.MINUS, "-", 0);
				NextWS();
				break;
			case '*':
				tok = new Token(Tokens.MUL, "*", 0);
				NextWS();
				break;
			case '/':
				tok = new Token(Tokens.DIV, "/", 0);
				NextWS();
				break;
			case '%':
				tok = new Token(Tokens.MOD, "%", 0);
				NextWS();
				break;
			case '&':
				tok = new Token(Tokens.AND, "&", 0);
				NextWS();
				break;
			case '|':
				tok = new Token(Tokens.OR, "|", 0);
				NextWS();
				break;
			case '^':
				tok = new Token(Tokens.XOR, "^", 0);
				NextWS();
				break;
			case '(':
				tok = new Token(Tokens.LPAREN, "(", 0);
				NextWS();
				break;
			case ')':
				tok = new Token(Tokens.RPAREN, ")", 0);
				NextWS();
				break;
			case ',':
				tok = new Token(Tokens.COMMA, ",", 0);
				NextWS();
				break;
			case '{':
				tok = new Token(Tokens.LBRACE, "{", 0);
				NextWS();
				break;
			case '}':
				tok = new Token(Tokens.RBRACE, "}", 0);
				NextWS();
				break;
			case '[':
				tok = new Token(Tokens.LBRACK, "[", 0);
				NextWS();
				break;
			case ']':
				tok = new Token(Tokens.RBRACK, "]", 0);
				NextWS();
				break;
			case ';':
				tok = new Token(Tokens.SEMICOLON, ";", 0);
				NextWS();
				break;
			case '=':
				NextWS();
				if (Look() == '=') {
					tok = new Token(Tokens.EQUAL, "==", 0);
					NextWS();
				} else {
					tok = new Token(Tokens.ASSIGNMENT, "=", 0);
				}

				break;
			case '>':
				NextWS();
				if (Look() == '=') {
					tok = new Token(Tokens.GEQ, ">=", 0);
					NextWS();
				} else {
					tok = new Token(Tokens.GREATER, ">", 0);

				}
				break;
			case '<':
				NextWS();
				if (Look() == '=') {
					tok = new Token(Tokens.LEQ, "<=", 0);
					NextWS();
				} else {
					tok = new Token(Tokens.LESS, "<", 0);
				}
				break;
			case '!':
				Next();
				if (Look() == '=') {
					tok = new Token(Tokens.NEQ, "!=", 0);
					NextWS();
				} else {
					System.out.println("Syntax Error: ! without =");
				}
				break;
			default:
				System.out.println("Syntax Error: Unexpected character '" + Look() + "'");
				break;
			}

		}
		return tok;
	}

	private String GetHex()
	{
		String ret = "";
		while (Character.isDigit(Look()) || ((Look() >= 'A') && (Look() <= 'F'))|| ((Look() >= 'a') && (Look() <= 'f'))) {
			ret += Look();
			Next();
		}
		SkipWS();
		return ret;
	}

	private Token GetString()
	{
		Token tok = null;
		String s = "";
		if (Look() == '"') {
			Next();
			while (Look() != '"') {
				/*
				 * if (Look() == '\\') { Next(); switch (Look())
				 * { case '\\': s += '\\'; break; case 'n': s +=
				 * '\n'; break; case 'r': s += '\r'; break; case
				 * '0': s += '\0'; break; case '"': s += '"';
				 * break; default: System.out.println(
				 * "Unknown escape character sequence: \\" + s);
				 * break; } } else {
				 */
				s += Look();
				// }

				Next();
			}
			NextWS();
			tok = new Token(Tokens.STRING, s, 0);
		}
		return tok;
	}

	private Token Ident()
	{
		String id = GetName();
		if (id.equals("if")) {
			return new Token(Tokens.IF, id, 0);
		} else if (id.equals("else")) {
			return new Token(Tokens.ELSE, id, 0);
		} else if (id.equals("func")) {
			return new Token(Tokens.FUNC, id, 0);
		} else if (id.equals("var")) {
			return new Token(Tokens.VAR, id, 0);
		} else if (id.equals("peek")) {
			return new Token(Tokens.PEEK, id, 0);
		} else if (id.equals("poke")) {
			return new Token(Tokens.POKE, id, 0);
		} else if (id.equals("return")) {
			return new Token(Tokens.RETURN, id, 0);
		} else if (id.equals("while")) {
			return new Token(Tokens.WHILE, id, 0);
		} else if (id.equals("for")) {
			return new Token(Tokens.FOR, id, 0);
		} else {
			return new Token(Tokens.IDENT, id, 0);
		}
	}
}
